/*
Uses symplectic euler integrator.
Uses one block with one thread per masspoint 
*/

#include <cuda_runtime.h>
#include <device_launch_parameters.h>
#include <stdio.h>
#include <math.h>

#include "graphics_cuda.cuh"
#include "constants.h"

float4 *x_new;
float3 *p_new;
float3 *d_f;


__global__ void test_kernel(float4 *dx, float3 *dp){
	printf("cuda momentum particle %i: %f %f %f\n", blockIdx.x, dp[blockIdx.x].x, dp[blockIdx.x].y, dp[blockIdx.x].z);
}


__device__ float3 force_pair(float4 xi, float4 xj){

	float3 dist;
	dist.x = xi.x - xj.x;
	dist.y = xi.y - xj.y;
	dist.z = xi.z - xj.z;

	float r = sqrtf(dist.x*dist.x + dist.y*dist.y + dist.z*dist.z + EPS2);
	float r3 = r*r*r;

	float force_mag;
	force_mag = GRAV * xi.w * xj.w;
	force_mag = force_mag / r3;

	float3 force;
	force.x = force_mag * dist.x;
	force.y = force_mag * dist.y;
	force.z = force_mag * dist.z;

	return force;
}

__global__ void euler_update(float4* x, float3* p, float4* x_new, float3* p_new, int n, float t){

	int idx = (blockIdx.x * BLOCKSIZE) + threadIdx.x;
	if (idx >= NUMPOINTS) return; //safety measure for when number of points is no multiple of 1024
	float3 total_force;
	total_force.x = 0;
	total_force.y = 0;
	total_force.z = 0;

	//printf("total force beginning, kernel %d: %f", idx, total_force.x);
	
	

	//printf("total force kernel %d: %f\n", idx, total_force.x);

	x_new[idx].x = x[idx].x + t * p[idx].x / x[idx].w;
	x_new[idx].y = x[idx].y + t * p[idx].y / x[idx].w;
	x_new[idx].z = x[idx].z + t * p[idx].z / x[idx].w;
	x_new[idx].w = x[idx].w;

	for (int i = 0; i < n; i++){
		float3 temp_force = force_pair(x_new[idx], x_new[i]);
		//printf("temp force kernel %d: %f\n", idx, temp_force.x);
		total_force.x += temp_force.x;
		total_force.y += temp_force.y;
		total_force.z += temp_force.z;

	}

	p_new[idx].x = p[idx].x - t * total_force.x;
	p_new[idx].y = p[idx].y - t * total_force.y;
	p_new[idx].z = p[idx].z - t * total_force.z;
	//printf("particle %i momentum: %f %f %f\n", idx, p[idx].x, p[idx].y, p[idx].z);
	

}

void integration_step_euler(float4* d_x, float3* d_p, int n, float t){
	//calculate new positions and store in seperate arrays for thread safety
	int numblocks = NUMPOINTS / BLOCKSIZE;
	if (NUMPOINTS%BLOCKSIZE != 0) numblocks += 1;
	euler_update<<<numblocks,BLOCKSIZE >>>(d_x, d_p, x_new, p_new, n, t);
	cudaDeviceSynchronize();

	//swap the memory locations of new and old positions
	float4* x_temp;
	float3* p_temp;
	x_temp = d_x;
	p_temp = d_p;
	d_x = x_new;
	d_p = p_new;
	x_new = x_temp;
	p_new = p_temp;

}

void init_cuda(){
	cudaMalloc(&x_new, NUMPOINTS*sizeof(float4));
	cudaMalloc(&p_new, NUMPOINTS*sizeof(float3));
	cudaMalloc(&d_f, NUMPOINTS*sizeof(float3));


}


//places two masses and integrates with 1000 timesteps over one second. prints the end positions and momenta
void simple_test(){
	int n = 2;
	float t = 0.001f;

	float4 x[2];
	x[0].x = 0;
	x[0].y = 0;
	x[0].z = 0;
	x[0].w = 1;

	x[1].x = 1000;
	x[1].y = 0;
	x[1].z = 0;
	x[1].w = 1;

	float3 p[2];
	p[0].x = 0;
	p[0].y = 0;
	p[0].z = 0;
	p[1].x = 0;
	p[1].y = 0;
	p[1].z = 0;


	//float4* d_x;
	//float3* d_p;
	float4 *d_x;
	float3 *d_p;
	cudaMalloc((void**)&d_x, 2 * sizeof(float4));
	cudaMalloc((void**)&d_p, 2 * sizeof(float3));
	cudaMemcpy(d_x, x, 2 * sizeof(float4), cudaMemcpyHostToDevice);
	cudaMemcpy(d_p, p, 2 * sizeof(float3), cudaMemcpyHostToDevice);

	for (int i = 0; i < 1000; i++){
		integration_step_euler(d_x, d_p, n, t);
	}

	cudaMemcpy(x, d_x, 2 * sizeof(float4), cudaMemcpyDeviceToHost);
	cudaMemcpy(p, d_p, 2 * sizeof(float3), cudaMemcpyDeviceToHost);

	printf("x1= %f\n", x[1].x);
	printf("p1= %f\n", p[1].x);
	printf("x0= %f\n", x[0].x);
	printf("p0= %f\n", p[0].x);



}

void test_cuda_kernel(float4 *dx, float3 *dp){
	test_kernel << <2, 1 >> >(dx, dp);
}

