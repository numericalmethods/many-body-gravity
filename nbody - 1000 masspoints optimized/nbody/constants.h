#ifndef CONST_HEADER
#define CONST_HEADER

#define NUMPOINTS 1024
#define BLOCKSIZE 256

#define EPS2 1e-5f
//#define GRAV 6.67384e-11 //real value for SI system
#define GRAV 1.0/5000 //test value to get visible effect with small masses


#endif