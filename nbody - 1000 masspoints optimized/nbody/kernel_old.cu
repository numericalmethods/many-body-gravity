
#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include <stdio.h>

#define EPS 1e-5
#define GRAV 6.67384e-11

// updates the positions of all masspoints according to the symplectic euler integrator
// call with a blockDim equal to n
// d_m holds the masses, d_x the current positions, d_p the current momenta, d_f holds the force vectors.
// t specifies the length of the timestep
__global__ void symp_euler_update(double *d_m, double3 *d_x, double3 *d_p, double3 *d_f, double t) {

	d_x[blockIdx.x].x = d_x[blockIdx.x].x + t * d_p[blockIdx.x].x / d_m[blockIdx.x];
	d_p[blockIdx.x].x = d_p[blockIdx.x].x - t * d_f[blockIdx.x].x;

	d_x[blockIdx.x].y = d_x[blockIdx.x].y + t * d_p[blockIdx.x].y / d_m[blockIdx.x];
	d_p[blockIdx.x].y = d_p[blockIdx.x].y - t * d_f[blockIdx.x].y;

	d_x[blockIdx.x].z = d_x[blockIdx.x].z + t * d_p[blockIdx.x].z / d_m[blockIdx.x];
	d_p[blockIdx.x].z = d_p[blockIdx.x].z - t * d_f[blockIdx.x].z;

}


__device__ double3 force_pair(double *d_m, double3 *d_x, double3 *d_f){
	
	double3 dist;
	dist.x = d_x[blockIdx.x].x - d_x[threadIdx.x].x;
	dist.y = d_x[blockIdx.x].y - d_x[threadIdx.x].y;
	dist.z = d_x[blockIdx.x].z - d_x[threadIdx.x].z;

	double force_mag;
	force_mag = GRAV * d_m[blockIdx.x] * d_m[threadIdx.x] / (sqrt(dist.x**x + dist.y**2 + dist.z**2 + EPS**2))**3

		d_f[blockIdx.x].x =

}

//executes one timestep with the symplectic euler integrator
void symp_euler_int(double *d_m, double3 *d_x, double3 *d_p, double3 *d_xnew, double3 *d_pnew, double t, int n){


	force_calc(*d_m, *d_x, *d_f);
	symp_euler_update << <n, 1 >> >(*d_m, *d_x, *d_p, *d_f, t);


}

void force_calc(double *d_m, double *d_x, double *d_f){




}