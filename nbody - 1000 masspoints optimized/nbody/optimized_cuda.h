#ifndef OPT_CUDA
#define OPT_CUDA
#include <cuda_runtime.h>

void init_cuda();

void integration_step_4th_order(float4* d_x, float3* d_p, float t);

#endif