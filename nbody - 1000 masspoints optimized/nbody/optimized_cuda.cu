/**************************************************************************************************

Integration of 4th order, using tiles.

****************************************************************************************************/

#include <cuda_runtime.h>
#include <device_launch_parameters.h>
#include <stdio.h>
#include <math.h>

#include "graphics_cuda.cuh"
#include "constants.h"

__device__ float3 d_f[NUMPOINTS];
__shared__ float4 shPos[BLOCKSIZE];

__device__ float3 force_pair(float4 xi, float4 xj){

	float3 dist;
	dist.x = xi.x - xj.x;
	dist.y = xi.y - xj.y;
	dist.z = xi.z - xj.z;

	float r = sqrtf(dist.x*dist.x + dist.y*dist.y + dist.z*dist.z + EPS2);
	float r3 = r*r*r;

	float force_mag;
	force_mag = GRAV * xi.w * xj.w;
	force_mag = force_mag / r3;

	float3 force;
	force.x = force_mag * dist.x;
	force.y = force_mag * dist.y;
	force.z = force_mag * dist.z;

	return force;
}

//computes all force interatctions in a given tile.
__device__ void force_tile(float4* x, float4 *shPos){
	float3 temp_force;
	int idx = (blockIdx.x * BLOCKSIZE) + threadIdx.x;
	float4 mypos = x[idx];

	for (int i = 0; i < BLOCKSIZE; i++){
		temp_force = force_pair(mypos, shPos[i]);
		//if (idx == 1) printf("force on particle 1 by particle %i: %f %f %f \n", i, temp_force.x, temp_force.y, temp_force.z);
		d_f[idx].x = d_f[idx].x + temp_force.x;
		d_f[idx].y = d_f[idx].y + temp_force.y;
		d_f[idx].z = d_f[idx].z + temp_force.z;
	}

}

__global__ void total_force(float4* x){
		
	//__shared__ float4 sh_pos[BLOCKSIZE]; //globally declared.

	int idx = (blockIdx.x * BLOCKSIZE) + threadIdx.x;
	if (idx < NUMPOINTS){
		d_f[idx].x = 0;
		d_f[idx].y = 0;
		d_f[idx].z = 0;
	}
	for (int i = 0; i < NUMPOINTS; i = i + BLOCKSIZE){
		if (i + threadIdx.x < NUMPOINTS){
			//printf("copying particle %i", i + threadIdx.x);
			shPos[threadIdx.x] = x[i + threadIdx.x]; //Each thread copies one element from the global memory to the shared memory
		}
		else {
			float4 temp_pos;
			temp_pos.x = 0;
			temp_pos.y = 0;
			temp_pos.z = 0;
			temp_pos.w = 0;
			shPos[threadIdx.x] = temp_pos;
		}
		__syncthreads();
		//printf("Shared Position %i: %f %f %f \n", threadIdx.x, shPos[threadIdx.x].x, shPos[threadIdx.x].y, shPos[threadIdx.x].z);
		/*if (threadIdx.x == 0){
			for (int i = 0; i < BLOCKSIZE; i++){
				printf("shared Position %i: %f %f %f %f\n", i, shPos[i].x, shPos[i].y, shPos[i].z, shPos[i].w);
			}
		}*/
		if (i + threadIdx.x < NUMPOINTS){
			force_tile(x, shPos);
		}
		__syncthreads();
	}
	
}

//DEPRECATED!!!!! DO NOT USE!!!!
/*__global__ void update_4th_order(float4* x, float3* p, float4* x_new, float3* p_new, int n, float t, int step){

	int idx = (blockIdx.x * BLOCKSIZE) + threadIdx.x;

	float m = x[idx].w;
    
	//first order
	if (step == 1){
		x[idx].x = x[idx].x + 1 / (2 * (2 - cbrtf(2)))*t * 1 / m * p[idx].x;
		x[idx].y = x[idx].y + 1 / (2 * (2 - cbrtf(2)))*t * 1 / m * p[idx].y;
		x[idx].z = x[idx].z + 1 / (2 * (2 - cbrtf(2)))*t * 1 / m * p[idx].z;
		__syncthreads();
		total_force(x);
		__syncthreads();
		p[idx].x = p[idx].x - 1 / (2 - cbrtf(2))*t*d_f[idx].x;
		p[idx].y = p[idx].y - 1 / (2 - cbrtf(2))*t*d_f[idx].y;
		p[idx].z = p[idx].z - 1 / (2 - cbrtf(2))*t*d_f[idx].z;
		__syncthreads();
	}
	//second order
	if (step == 2){
		x[idx].x = x[idx].x + (1 - cbrtf(2)) / (2 * (2 - cbrtf(2)))*t * 1 / m * p[idx].x;
		x[idx].y = x[idx].y + (1 - cbrtf(2)) / (2 * (2 - cbrtf(2)))*t * 1 / m * p[idx].y;
		x[idx].z = x[idx].z + (1 - cbrtf(2)) / (2 * (2 - cbrtf(2)))*t * 1 / m * p[idx].z;
		__syncthreads();
		total_force(x);
		__syncthreads();
		p[idx].x = p[idx].x - 1 / (2 - cbrtf(2))*t*d_f[idx].x;
		p[idx].y = p[idx].y - 1 / (2 - cbrtf(2))*t*d_f[idx].y;
		p[idx].z = p[idx].z - 1 / (2 - cbrtf(2))*t*d_f[idx].z;
		__syncthreads();
	}



}*/

__global__ void update_positions(float4* x, float3* p, float t, int step){

	int idx = (blockIdx.x * BLOCKSIZE) + threadIdx.x;
	if (idx >= NUMPOINTS) return; //if responsible for non existant particle: do nothing.
	float m = x[idx].w;

	if (step == 1 || step == 4){
		x[idx].x = x[idx].x + 1 / (2 * (2 - cbrtf(2)))*t * 1 / m * p[idx].x;
		x[idx].y = x[idx].y + 1 / (2 * (2 - cbrtf(2)))*t * 1 / m * p[idx].y;
		x[idx].z = x[idx].z + 1 / (2 * (2 - cbrtf(2)))*t * 1 / m * p[idx].z;
	}
	if (step == 2 || step == 3){
		x[idx].x = x[idx].x + (1 - cbrtf(2)) / (2 * (2 - cbrtf(2)))*t * 1 / m * p[idx].x;
		x[idx].y = x[idx].y + (1 - cbrtf(2)) / (2 * (2 - cbrtf(2)))*t * 1 / m * p[idx].y;
		x[idx].z = x[idx].z + (1 - cbrtf(2)) / (2 * (2 - cbrtf(2)))*t * 1 / m * p[idx].z;
	}

}

__global__ void update_momenta(float4* x, float3* p, float t, int step){
	int idx = (blockIdx.x * BLOCKSIZE) + threadIdx.x;
	if (idx >= NUMPOINTS) return; //if responsible for non existant particle: do nothing.
	if (step == 1 || step == 3){
		p[idx].x = p[idx].x - 1 / (2 - cbrtf(2))*t*d_f[idx].x;
		p[idx].y = p[idx].y - 1 / (2 - cbrtf(2))*t*d_f[idx].y;
		p[idx].z = p[idx].z - 1 / (2 - cbrtf(2))*t*d_f[idx].z;
	}
	if (step == 2) {
		p[idx].x = p[idx].x + cbrtf(2) / (2 - cbrtf(2))*t*d_f[idx].x;
		p[idx].y = p[idx].y + cbrtf(2) / (2 - cbrtf(2))*t*d_f[idx].y;
		p[idx].z = p[idx].z + cbrtf(2) / (2 - cbrtf(2))*t*d_f[idx].z;
	}
}

void init_cuda(){
	//cudaMalloc(&d_f, NUMPOINTS*sizeof(float3));
	//printf("Cuda Initialisiert.\n");

	cudaFuncSetCacheConfig(update_positions, cudaFuncCachePreferL1);
	cudaFuncSetCacheConfig(update_momenta, cudaFuncCachePreferL1);
	cudaFuncSetCacheConfig(total_force, cudaFuncCachePreferShared);	

}

void integration_step_4th_order(float4* d_x, float3* d_p, float t){
	int numblocks = NUMPOINTS / BLOCKSIZE;
	if (NUMPOINTS%BLOCKSIZE != 0) numblocks += 1;

	//steps one through three
	for (int i = 1; i < 4; i++){
		update_positions << <numblocks, BLOCKSIZE >> >(d_x, d_p, t, i);
		cudaDeviceSynchronize();
		total_force << <numblocks, BLOCKSIZE >> >(d_x);
		cudaDeviceSynchronize();
		update_momenta << <numblocks, BLOCKSIZE >> >(d_x, d_p, t, i);
		cudaDeviceSynchronize();
	}

	//step four (d4 = 0)
	update_positions << <numblocks, BLOCKSIZE >> >(d_x, d_p, t, 4);



}