#ifndef CONST_HEADER
#define CONST_HEADER

#define NUMPOINTS 2
#define BLOCKSIZE 512

#define EPS2 1e-9f
//#define GRAV 6.67384e-11 //real value for SI system
#define GRAV 1.0/5000 //test value to get visible effect with small masses


#endif