/**************************************************************************************************

Integration of 4th order, using tiles.

****************************************************************************************************/

#include <cuda_runtime.h>
#include <device_launch_parameters.h>
#include <stdio.h>
#include <math.h>

#include "graphics_cuda.cuh"
#include "constants.h"

__device__ float3 d_f[NUMPOINTS];
__shared__ float4 shPos[BLOCKSIZE];

__device__ float3 force_pair(float4 xi, float4 xj){

	float3 dist;
	float3 force;
	dist.x = xi.x - xj.x;
	dist.y = xi.y - xj.y;
	dist.z = xi.z - xj.z;
	//initialize force vector
	force.x = 0;
	force.y = 0;
	force.z = 0;

	float r;
	r = sqrtf(dist.x*dist.x + dist.y*dist.y + dist.z*dist.z);
	//minimum image convention:
	if (r > 1){
		//search adjacent cells for fitting image
		int sign_diff[3] = { signbit(xi.x) - signbit(xj.x), signbit(xi.y) - signbit(xj.y), signbit(xi.z) - signbit(xj.z) };
		if (sign_diff[0] == 0){
			if (sign_diff[1] == 0){
				if (sign_diff[2] == 0){
					return force;
				}
				else{
					//shift z-Axis
					if (xi.z < 0){
						xj.z = xj.z - 2;
					}
					else{
						xj.z = xj.z + 2;
					}
					dist.z = xi.z - xj.z;
					r = sqrtf(dist.x*dist.x + dist.y*dist.y + dist.z*dist.z);
					if (r > 1) return force; //returns 0 if out of range, else falls through to calculation
				}
			}
			else{
				//shift y-axis
				if (xi.y < 0){
					xj.y = xj.y - 2;
				}
				else{
					xj.y = xj.y + 2;
				}
				dist.y = xi.y - xj.y;
				r = sqrtf(dist.x*dist.x + dist.y*dist.y + dist.z*dist.z);
				if (r>1){ //tries different shifts if out of range, else falls through to calculation
					//shift y- and z- axis
					if (xi.z < 0){
						xj.z = xj.z - 2;
					}
					else{
						xj.z = xj.z + 2;
					}
					dist.z = xi.z - xj.z;
					r = sqrtf(dist.x*dist.x + dist.y*dist.y + dist.z*dist.z);
					if (r>1){
						//shift only z-axis
						if (xi.y > 0){
							xj.y = xj.y - 2;
						}
						else{
							xj.y = xj.y + 2;
						}
						dist.y = xi.y - xj.y;
						r = sqrtf(dist.x*dist.x + dist.y*dist.y + dist.z*dist.z);
						if (r > 1) return force; //all possibilities exhausted.

					}
				}
			}
		}
		else{
			//shift x-axis
			if (xi.x < 0){
				xj.x = xj.x - 2;
			}
			else{
				xj.x = xj.x + 2;
			}
			dist.x = xi.x - xj.x;
			r = sqrtf(dist.x*dist.x + dist.y*dist.y + dist.z*dist.z);
			if (r>1){
				//shift x- and y- axis
				if (xi.y < 0){
					xj.y = xj.y - 2;
				}
				else{
					xj.y = xj.y + 2;
				}
				dist.y = xi.y - xj.y;
				r = sqrtf(dist.x*dist.x + dist.y*dist.y + dist.z*dist.z);
				if (r>1){ //tries different shifts if out of range, else falls through to calculation
					//shift x- y- and z- axis
					if (xi.z < 0){
						xj.z = xj.z - 2;
					}
					else{
						xj.z = xj.z + 2;
					}
					dist.z = xi.z - xj.z;
					r = sqrtf(dist.x*dist.x + dist.y*dist.y + dist.z*dist.z);
					if (r>1){
						//shift x- and z-axis
						if (xi.y > 0){
							xj.y = xj.y - 2;
						}
						else{
							xj.y = xj.y + 2;
						}
						dist.y = xi.y - xj.y;
						r = sqrtf(dist.x*dist.x + dist.y*dist.y + dist.z*dist.z);
						if (r > 1){
							//shift z-axis
							if (xi.x > 0){
								xj.x = xj.x - 2;
							}
							else{
								xj.x = xj.x + 2;
							}
							dist.x = xi.x - xj.x;
							r = sqrtf(dist.x*dist.x + dist.y*dist.y + dist.z*dist.z);
							if (r > 1){
								//shift y- and z- axis
								if (xi.y < 0){
									xj.y = xj.y - 2;
								}
								else{
									xj.y = xj.y + 2;
								}
								dist.y = xi.y - xj.y;
								r = sqrtf(dist.x*dist.x + dist.y*dist.y + dist.z*dist.z);
								if (r >1){
									//shift y- axis
									if (xi.z > 0){
										xj.z = xj.z - 2;
									}
									else{
										xj.z = xj.z + 2;
									}
									dist.z = xi.z - xj.z;
									r = sqrtf(dist.x*dist.x + dist.y*dist.y + dist.z*dist.z);
									if (r > 1) return force; //every possibility exhausted.
								}

							}
						}
					}
				}
			}
		}
	

		
	}
	//add smoothing parameter to distance
	r = sqrtf(r*r + EPS2);
	float r3 = r*r*r;

	float force_mag;
	force_mag = GRAV * xi.w * xj.w;
	force_mag = force_mag / r3;


	force.x = force_mag * dist.x;
	force.y = force_mag * dist.y;
	force.z = force_mag * dist.z;

	return force;
}

//computes all force interatctions in a given tile.
__device__ void force_tile(float4* x, float4 *shPos){
	float3 temp_force;
	int idx = (blockIdx.x * BLOCKSIZE) + threadIdx.x;
	float4 mypos = x[idx];

	for (int i = 0; i < BLOCKSIZE; i++){
		temp_force = force_pair(mypos, shPos[i]);
		//if (idx == 1) printf("force on particle 1 by particle %i: %f %f %f \n", i, temp_force.x, temp_force.y, temp_force.z);
		d_f[idx].x = d_f[idx].x + temp_force.x;
		d_f[idx].y = d_f[idx].y + temp_force.y;
		d_f[idx].z = d_f[idx].z + temp_force.z;
	}

}

__global__ void total_force(float4* x){
		
	//__shared__ float4 sh_pos[BLOCKSIZE]; //globally declared.

	int idx = (blockIdx.x * BLOCKSIZE) + threadIdx.x;
	if (idx < NUMPOINTS){
		d_f[idx].x = 0;
		d_f[idx].y = 0;
		d_f[idx].z = 0;
	}
	for (int i = 0; i < NUMPOINTS; i = i + BLOCKSIZE){
		if (i + threadIdx.x < NUMPOINTS){
			//printf("copying particle %i", i + threadIdx.x);
			shPos[threadIdx.x] = x[i + threadIdx.x]; //Each thread copies one element from the global memory to the shared memory
		}
		else {
			float4 temp_pos;
			temp_pos.x = 0;
			temp_pos.y = 0;
			temp_pos.z = 0;
			temp_pos.w = 0;
			shPos[threadIdx.x] = temp_pos;
		}
		__syncthreads();
		//printf("Shared Position %i: %f %f %f \n", threadIdx.x, shPos[threadIdx.x].x, shPos[threadIdx.x].y, shPos[threadIdx.x].z);
		/*if (threadIdx.x == 0){
			for (int i = 0; i < BLOCKSIZE; i++){
				printf("shared Position %i: %f %f %f %f\n", i, shPos[i].x, shPos[i].y, shPos[i].z, shPos[i].w);
			}
		}*/
		if (i + threadIdx.x < NUMPOINTS){
			force_tile(x, shPos);
		}
		__syncthreads();
	}
	
}

//DEPRECATED!!!!! DO NOT USE!!!!
/*__global__ void update_4th_order(float4* x, float3* p, float4* x_new, float3* p_new, int n, float t, int step){

	int idx = (blockIdx.x * BLOCKSIZE) + threadIdx.x;

	float m = x[idx].w;
    
	//first order
	if (step == 1){
		x[idx].x = x[idx].x + 1 / (2 * (2 - cbrtf(2)))*t * 1 / m * p[idx].x;
		x[idx].y = x[idx].y + 1 / (2 * (2 - cbrtf(2)))*t * 1 / m * p[idx].y;
		x[idx].z = x[idx].z + 1 / (2 * (2 - cbrtf(2)))*t * 1 / m * p[idx].z;
		__syncthreads();
		total_force(x);
		__syncthreads();
		p[idx].x = p[idx].x - 1 / (2 - cbrtf(2))*t*d_f[idx].x;
		p[idx].y = p[idx].y - 1 / (2 - cbrtf(2))*t*d_f[idx].y;
		p[idx].z = p[idx].z - 1 / (2 - cbrtf(2))*t*d_f[idx].z;
		__syncthreads();
	}
	//second order
	if (step == 2){
		x[idx].x = x[idx].x + (1 - cbrtf(2)) / (2 * (2 - cbrtf(2)))*t * 1 / m * p[idx].x;
		x[idx].y = x[idx].y + (1 - cbrtf(2)) / (2 * (2 - cbrtf(2)))*t * 1 / m * p[idx].y;
		x[idx].z = x[idx].z + (1 - cbrtf(2)) / (2 * (2 - cbrtf(2)))*t * 1 / m * p[idx].z;
		__syncthreads();
		total_force(x);
		__syncthreads();
		p[idx].x = p[idx].x - 1 / (2 - cbrtf(2))*t*d_f[idx].x;
		p[idx].y = p[idx].y - 1 / (2 - cbrtf(2))*t*d_f[idx].y;
		p[idx].z = p[idx].z - 1 / (2 - cbrtf(2))*t*d_f[idx].z;
		__syncthreads();
	}



}*/

__global__ void update_positions(float4* x, float3* p, float t, int step){

	int idx = (blockIdx.x * BLOCKSIZE) + threadIdx.x;
	if (idx >= NUMPOINTS) return; //if responsible for non existant particle: do nothing.
	float m = x[idx].w;

	if (step == 1 || step == 4){
		x[idx].x = x[idx].x + 1 / (2 * (2 - cbrtf(2)))*t * 1 / m * p[idx].x;
		x[idx].y = x[idx].y + 1 / (2 * (2 - cbrtf(2)))*t * 1 / m * p[idx].y;
		x[idx].z = x[idx].z + 1 / (2 * (2 - cbrtf(2)))*t * 1 / m * p[idx].z;
	}
	if (step == 2 || step == 3){
		x[idx].x = x[idx].x + (1 - cbrtf(2)) / (2 * (2 - cbrtf(2)))*t * 1 / m * p[idx].x;
		x[idx].y = x[idx].y + (1 - cbrtf(2)) / (2 * (2 - cbrtf(2)))*t * 1 / m * p[idx].y;
		x[idx].z = x[idx].z + (1 - cbrtf(2)) / (2 * (2 - cbrtf(2)))*t * 1 / m * p[idx].z;
	}

	//Periodic boundary condition:
	if (x[idx].x < -1){
		x[idx].x = x[idx].x + 2;
	}
	if (x[idx].x > 1){
		x[idx].x = x[idx].x - 2;
	}
	if (x[idx].y < -1){
		x[idx].y = x[idx].y + 2;
	}
	if (x[idx].y > 1){
		x[idx].y = x[idx].y - 2;
	}
	if (x[idx].z < -1){
		x[idx].z = x[idx].z + 2;
	}
	if (x[idx].z > 1){
		x[idx].z = x[idx].z - 2;
	}

}

__global__ void update_momenta(float4* x, float3* p, float t, int step){
	int idx = (blockIdx.x * BLOCKSIZE) + threadIdx.x;
	if (idx >= NUMPOINTS) return; //if responsible for non existant particle: do nothing.
	if (step == 1 || step == 3){
		p[idx].x = p[idx].x - 1 / (2 - cbrtf(2))*t*d_f[idx].x;
		p[idx].y = p[idx].y - 1 / (2 - cbrtf(2))*t*d_f[idx].y;
		p[idx].z = p[idx].z - 1 / (2 - cbrtf(2))*t*d_f[idx].z;
	}
	if (step == 2) {
		p[idx].x = p[idx].x + cbrtf(2) / (2 - cbrtf(2))*t*d_f[idx].x;
		p[idx].y = p[idx].y + cbrtf(2) / (2 - cbrtf(2))*t*d_f[idx].y;
		p[idx].z = p[idx].z + cbrtf(2) / (2 - cbrtf(2))*t*d_f[idx].z;
	}
}

void init_cuda(){
	//cudaMalloc(&d_f, NUMPOINTS*sizeof(float3));
	//printf("Cuda Initialisiert.\n");

	cudaFuncSetCacheConfig(update_positions, cudaFuncCachePreferL1);
	cudaFuncSetCacheConfig(update_momenta, cudaFuncCachePreferL1);
	cudaFuncSetCacheConfig(total_force, cudaFuncCachePreferShared);	

}

void integration_step_4th_order(float4* d_x, float3* d_p, float t){
	int numblocks = NUMPOINTS / BLOCKSIZE;
	if (NUMPOINTS%BLOCKSIZE != 0) numblocks += 1;

	//steps one through three
	for (int i = 1; i < 4; i++){
		update_positions << <numblocks, BLOCKSIZE >> >(d_x, d_p, t, i);
		cudaDeviceSynchronize();
		total_force << <numblocks, BLOCKSIZE >> >(d_x);
		cudaDeviceSynchronize();
		update_momenta << <numblocks, BLOCKSIZE >> >(d_x, d_p, t, i);
		cudaDeviceSynchronize();
	}

	//step four (d4 = 0)
	update_positions << <numblocks, BLOCKSIZE >> >(d_x, d_p, t, 4);



}

float3 calculate_energies(float4* d_x, float3* d_p){
	float3 energy;
	energy.x = 0;
	energy.y = 0;
	energy.z = 0;
	for (int i = 0; i < NUMPOINTS; i++){
		//potential energy:
		for (int j = i+1; j < NUMPOINTS; j++){
			float dist = (d_x[i].x - d_x[j].x)*(d_x[i].x - d_x[j].x) + (d_x[i].y - d_x[j].y)*(d_x[i].y - d_x[j].y) + (d_x[i].z - d_x[j].z)*(d_x[i].z - d_x[j].z);
			energy.y += GRAV * d_x[i].w * d_x[j].w / sqrtf(dist);
		}
		//kinetic energy:
		energy.z += (d_p[i].x*d_p[i].x + d_p[i].y*d_p[i].y + d_p[i].z*d_p[i].z) / (2 * d_x[i].w);
	}
	//total energy:
	energy.x = - energy.y + energy.z;
	printf("Energies: %f %f %f \n", energy.x, energy.y, energy.z);
	return energy;
	

}