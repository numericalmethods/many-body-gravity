#include <stdio.h>
#include <Windows.h>
#include <GL/glew.h>
#include <GL/freeglut.h>
#include "graphics_cuda.cuh"
#include "cuda_runtime.h"
#include <device_launch_parameters.h>
#include "constants.h"
#include "optimized_cuda.h"
#include <math.h>
#include <stdlib.h>
#include <time.h>

GLuint VBO;
float3 *d_momenta;
float4 *d_vertices;
float4 vertices[NUMPOINTS];
float3 momenta[NUMPOINTS];




void init_positions(){

	srand((unsigned)time(NULL));


	for (int i = 0; i < NUMPOINTS; i++){
		vertices[i].x = (float)rand() / (float)RAND_MAX - 0.5f;
		vertices[i].y = (float)rand() / (float)RAND_MAX - 0.5f;
		vertices[i].z = (float)rand() / (float)RAND_MAX - 0.5f;
		vertices[i].w = 1.0f;
		momenta[i].x = (float)rand() / (float)RAND_MAX - 0.5f;
		momenta[i].y = (float)rand() / (float)RAND_MAX - 0.5f;
		momenta[i].z = (float)rand() / (float)RAND_MAX - 0.5f;


	}

	//printf("starting position particle 0: %f %f %f\n", vertices[0].x, vertices[0].y, vertices[0].z);

	cudaMalloc(&d_vertices, NUMPOINTS * sizeof(float4));
	cudaMalloc(&d_momenta, NUMPOINTS * sizeof(float3));

	cudaMemcpy(d_vertices, vertices, NUMPOINTS * sizeof(float4), cudaMemcpyHostToDevice);
	cudaMemcpy(d_momenta, momenta, NUMPOINTS * sizeof(float3), cudaMemcpyHostToDevice);
}

void update_positions(int te){


	integration_step_4th_order(d_vertices, d_momenta, 0.030f);
	
	
	cudaMemcpy(vertices, d_vertices, NUMPOINTS * sizeof(float4), cudaMemcpyDeviceToHost);

	cudaMemcpy(momenta, d_momenta, NUMPOINTS * sizeof(float3), cudaMemcpyDeviceToHost);
	//printf("particle 0: %f %f %f \n", vertices[0].x, vertices[0].y, vertices[0].z);

	calculate_energies(vertices, momenta);

	glutPostRedisplay();
	glutTimerFunc(30, update_positions, 1);

}

static void RenderSceneCB() {
	glClear(GL_COLOR_BUFFER_BIT);



	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_DYNAMIC_DRAW);

	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(float4), 0);
	glDrawArrays(GL_POINTS, 0, NUMPOINTS);
	glDisableVertexAttribArray(0);

	glutSwapBuffers();
}

static void InitializeGlutCallbacks() {
	glutDisplayFunc(RenderSceneCB);
}


static void CreateVertexBuffer(){


	glGenBuffers(1, &VBO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_DYNAMIC_DRAW);

}

int init_graphics(int argc, char** argv) {

	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);
	glutInitWindowSize(700, 700);
	glutInitWindowPosition(0, 0);
	glutCreateWindow("N-body simulation");

	InitializeGlutCallbacks();

	GLenum res = glewInit();
	if (res != GLEW_OK) {
		fprintf(stderr, "Error initializing GLEW.\n");
		return 1;
	}

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	init_cuda();
	init_positions();


	CreateVertexBuffer();
	glutTimerFunc(30, update_positions, 1);
	glutMainLoop();
	return 0;

}

