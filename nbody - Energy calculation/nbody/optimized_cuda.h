#ifndef OPT_CUDA
#define OPT_CUDA
#include <cuda_runtime.h>

void init_cuda();

void integration_step_4th_order(float4* d_x, float3* d_p, float t);

//returns the total energy, kinetic energy and potential energy in the components of a float3
float3 calculate_energies(float4* d_x, float3* d_p);

#endif