#ifndef NAIVE_KERNEL
#define NAIVE_KERNEL
#include <cuda_runtime.h>

void integration_step_euler(float4* d_x, float3* d_p, int n, float t);

void simple_test();

void test_cuda_kernel(float4 *dx, float3 *dp);

#endif NAIVE_KERNEL