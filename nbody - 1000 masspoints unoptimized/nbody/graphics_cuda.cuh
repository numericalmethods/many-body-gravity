
/*
* Initializes GLUT
*/
int init_graphics(int argc, char** argv);

/*Initializes GLUT Callback methods*/
static void InitializeGlutCallbacks();

/*renders the contents of the window*/
static void RenderSceneCB();

/*creates the Vertex buffer in OpenGL*/
static void CreateVertexBuffer();

void init_positions();
